<footer id="footer">
	<div class="row">
		<div class="col-md-4 col-sm-4">
			<div class="footer-menu">
				<ul>
					<li class="active"><a href="<?php echo base_url('/index/page/home'); ?>">Home<i class="fa fa-chevron-right"></i></a></li>
					<li><a href="<?php echo base_url('/index/page/aboutus'); ?>">About us<i class="fa fa-chevron-right"></i></a></li>
					<li><a href="<?php echo base_url('/auth/login'); ?>">Login<i class="fa fa-chevron-right"></i></a></li>
					<li><a href="<?php echo base_url('/index/page/faq'); ?>">FAQ<i class="fa fa-chevron-right"></i></a></li>
					<li><a href="<?php echo base_url('/index/page/contact'); ?>">Contact Us<i class="fa fa-chevron-right"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="col-md-4 col-sm-4">
			<div class="newsletter">
				<h2>Our Newsletter</h2>
				<p>Sign up for our mailing list to get latest updates.</p>
				<form class="form-inline" action="subscribe.html">
					<div class="form-group">
						<input type="email" class="form-control" id="" placeholder="Enter Your Email" name="">
					</div>
					<div class="newsbtn"><input type="submit" class="btn btn-default" value="SUBSCRIBE"></div>
				</form>

			</div>
		</div>
		<div class="col-md-4 col-sm-4">
			<div class="instagram">
				<h2>instagram</h2>
				<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro15.png"></a>
			</div>
		</div>
	</div>
	<div class="footerbottom">
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="footerbottombox">
					<a><i class="fa fa-phone"></i></a>
					<!--<p>Call us Now :</p>
					<a>(+800) 0321-765-986</a>-->
					<p> WhatsApp Only</p>
					<a>+965 66723988</a>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
			<div class="footerbottombox center social">
					<a href="#"><i class="fa fa-share"></i></a> 
						<p>Follow Us On :</p>
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-google"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						</ul>
			</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="footerbottombox ">
					<a><i class="fa fa-envelope"></i></a>
					<p>Email :</p>
					<a>Info@shareyourbook.org</a>
				</div>
			</div>
		</div>
	</div>
</footer>
<div style="width: 100%; padding: 14px 0;text-transform: capitalize;font-size: 16px;background: #000;color: #fff;">
    <center> Powered By Channels Media  <a href="http://channelsmedia.com"><img src="http://shareyourbook.org/assets/books/images/cmlogo.png" style="width:30px;height:auto;"/></a></center>
    </div>
</div>
<script type="text/javascript" src="<?php echo $assets; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $assets; ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $assets; ?>js/owl.carousel.min.js"></script>
<script type="text/javascript">
    var base_url = "<?= base_url(); ?>";
</script>
<?php
if(isset($scripts) && is_array($scripts))
{
	foreach ($scripts as $key => $value) {
		?>
		<script src="<?php echo $value; ?>"></script>
		<?php
	}

}
?>
<?php if (isset($scripts) &&in_array('chat', $scripts)): ?>
	
<script>
 function sendMessage(e, obj) {
 	    if (e.keyCode === 13) {
          if($.trim($('body').find('input[name="message"]').val())=='')
          return false;	
          var form=new FormData();
          form.append('borrow',$('body').find('input[name="borrow"]').val());
          form.append('chat_id',$('body').find('input[name="chat_id"]').val());
          form.append('message',$('body').find('input[name="message"]').val());
          $('body').find('input[name="message"]').val('');
          $.ajax({
            	url: '<?= base_url() ?>books/chat_store',
            	type: 'POST',
            	dataType: 'json',
            	data: form,
            	processData: false,
          		contentType: false,
            })
            .done(function(response) {
            if(response.message=='success'){	
             $('#chat_id').append(response.html);
             $('input[name="chat_id"]').val(response.chat_id);
             $("#chat_id").scrollTop($('#chat_id').prop("scrollHeight"));
            }else{
              console.log(response.message); 	
            }
            })
            .fail(function() {
            	console.log("error");
            })
            .always(function() {
            	console.log("complete");
            });
        }
    }	
	$("#chat_id").scrollTop($('#chat_id').prop("scrollHeight"));
</script>
<?php endif ?>
</html>
