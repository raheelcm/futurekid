<div class="sidebar-collapse">
    <ul class="nav metismenu" id="side-menu">
        <li class="nav-header">
            <div class="dropdown profile-element">
            <a data-toggle="dropdown" class="dropdown-toggle" href="<?php echo base_url(); ?>#">
                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"> <?php
                    $user = $this->session->userdata('knet_login');
                    echo $user->first_name.' '.$user->last_name;
                    ?></strong>
                </span> <span class="text-muted text-xs block">
                <!-- <b class="caret"></b>--></span> </span> </a>
                <!-- <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <li><a href="<?php //echo base_url('/');; ?>profile.html">Profile</a></li>
                    <li><a href="<?php //echo base_url('/');; ?>contacts.html">Contacts</a></li>
                    <li><a href="<?php //echo base_url('/');; ?>mailbox.html">Mailbox</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php //echo base_url('/');; ?>login.html">Logout</a></li>
                </ul>-->
            </div>
            <div class="logo-element">
                IN+
            </div>
        </li>
         <li>
            <a href="<?php echo base_url('/admin/admin'); ?>"><i class="fa fa-circle"></i> <span class="nav-label">Dashboard</span></a>
        </li>
        <li>
            <a><i class="fa fa-book"></i> <span class="nav-label">Links</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li><a href="<?php echo base_url('/admin/book/all'); ?>">All Links</a></li>
                <li><a href="<?php echo base_url('/admin/book/create'); ?>">Create New Link</a></li>
            </ul>
        </li>
        <?php
        if($user->roleID == 1)
        {
            ?>
        <li>
                <a><i class="fa fa-tag"></i> <span class="nav-label">Gateways</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo base_url('/admin/tags/all'); ?> ">All Gateways</a></li>
                    <li><a href="<?php echo base_url('/admin/tags/create'); ?>">Add New Gateway</a></li>
                </ul>
        </li>


            <li>
                <li>
                    <a><i class="fa fa-edit"></i> <span class="nav-label">Clients</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo base_url('/admin/users'); ?>">All Clients</a></li>
                        <li><a href="<?php echo base_url('/admin/genres/create'); ?>">Add New Client</a></li>
                    </ul>
                </li>
                <?php
        }
        else
        {
            ?>
            <li>
                <?php
                if($user->api_status == 2)
                    {?>
            <a href="#"><i style="color: green;" class="fa fa-circle"></i> <span class="nav-label">Live</span></a>
            <?php
                    }
                    else
                    {
                     ?>
                     <a href="#"><i style="color: yellow;" class="fa fa-circle"></i> <span class="nav-label">Sandbox</span></a>
                     <?php   
                    }
                ?>
        </li>
            <?php
        }
            ?>
                
               
                
                
            </ul>
        </div>