<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sharebooks</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $assets; ?>css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $assets; ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $assets; ?>css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $assets; ?>css/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $assets; ?>css/style.css">
</head>
<body>
<div id="landing">
	<header id="header">
		<div class="container">
			<div id="logo"><a href="<?php echo base_url();?>"><img src="<?php echo $assets; ?>images/share-logo.png"></a></div>
			<div class="mainmenu">
				<div class="navbar-header">
		          	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"><i class="fa fa-bars"></i></button>
		        </div>
		        <div class="collapse navbar-collapse" id="navbar-menu"> 
		        	<ul>
						<li class="active"><a href="<?php echo base_url('/index/page/aboutus'); ?>">About us</a></li>
						<li><a href="<?php echo base_url('/index/page/faq'); ?>">FAQ</a></li>
						<li><a href="<?php echo base_url('/index/page/contact'); ?>">Contact</a></li>
					</ul>
					<ul class="signup">
						<li ><a href="<?php echo base_url('auth/register'); ?>">Sign up  </a></li>
						<li><a href="<?php echo base_url('auth/login'); ?>">Log In</a></li>
					</ul>
		        </div>
			</div>
		</div>
	</header>
	<div class="landingdetail">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="landingdet">
						<h1>Read more.<br>Spend less.</h1>
						<p>By sharing your paper books with others, you save money, save trees and read more books. Join our passionate community of book lovers, as we change the world and touch lives, one book at a time.</p>
						<a href="<?php echo base_url('index/home'); ?>">Get Started</a>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/Untitled-6.png"></a>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo $assets; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $assets; ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $assets; ?>js/owl.carousel.min.js"></script>
</html>