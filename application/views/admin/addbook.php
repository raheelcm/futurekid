<div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Create New Link</h3>
                            <!--<div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>-->
                        </div>
                        <div class="ibox-content">

                                <?php
                                if(isset($edit))
                                {
                                    ?>
                                    <form id="book_form" action="<?=base_url('admin/book/saveedit');?>" enctype="multipart/form-data" method="post" class="form-horizontal">
                                        <input type="hidden" name="bookID" value="<?=$edit['bookID']?>" >
                                    <?php

                                }
                                else
                                {
                                    ?>
                                    <form id="book_form" action="<?=base_url('admin/book/save');?>" enctype="multipart/form-data" method="post" class="form-horizontal">
                                    <?php

                                }
                                ?>
        <?php 
        $CI = get_instance();
        $this->load->view('flash'); ?>
                                <div class="form-group"><label for="invo" class="col-sm-2 control-label">Invoice No</label>

                                    <div class="col-sm-10"><input type="text" id="invo" name="invo_no" class="form-control"
                                        value="<?= (isset($edit['title']))?$edit['title']:''; ?>" 
                                        ></div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="ui-widget">
                                      <label for="amont" class="col-sm-2 control-label"> Amount: </label>
                                      <div class="col-sm-10"> <input class="form-control" id="amont" name="amount"
                                      value=""></div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                     <div class="ui-widget">
                                      <label for="name" class="col-sm-2 control-label">Name: </label>
                                     
                                     <div class="col-sm-10"> <input class="form-control" id="name" name="name"
                                      value="<?php
                                    if(isset($edit['authorID']))
                                    {
                                    echo $authorID = $CI->Book_model->getAuthorByID($edit['authorID'])->name;
                                    }
                                    
                                    ?>">
                                      <input id="author_id" type="hidden" name="author_id" 
                                    value=" " 
                                      ></div>
                                    </div> 
                                </div>
                                
                              
 
<div>
  <div id="log" ></div>
</div>
                                <div class="form-group">
                                     <div class="ui-widget">
                                      <label for="email" class="col-sm-2 control-label">Email: </label>
                                     
                                     <div class="col-sm-10"> <input class="form-control" id="email" name="email"
                                      value="<?php
                                    if(isset($edit['authorID']))
                                    {
                                    echo $authorID = $CI->Book_model->getAuthorByID($edit['authorID'])->name;
                                    }
                                    
                                    ?>">
                                      </div>
                                    </div> 
                                </div>
                                
                                <div class="form-group">
                                     <div class="ui-widget">
                                      <label for="phone" class="col-sm-2 control-label"> Phone No: </label>
                                     
                                     <div class="col-sm-10"><input class="form-control" id="phone" name="phone"
                                      value="<?php
                                    if(isset($edit['authorID']))
                                    {
                                    echo $authorID = $CI->Book_model->getAuthorByID($edit['authorID'])->name;
                                    }
                                    
                                    ?>">
                                      </div>
                                    </div> 
                                </div>
                                
                                <div class="form-group">
                                     <div class="ui-widget">
                                      <label for="sms" class="col-sm-2 control-label">Send SMS: </label>
                                     
                                     <div class="col-sm-10"> <input type="checkbox" class="form-control" id="sms" name="name"
                                      value="1">
                                      <input id="author_id" type="hidden" name="author_id" 
                                    value=" " 
                                      ></div>
                                    </div> 
                                </div>
                                


									
								</div>


                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <?php
                                if(isset($edit))
                                {
                                    ?>
                                    <button class="btn btn-white" type="submit">Cancel</button>
                                        <button class="btn btn-primary" type="button" onclick="payment_link()">Create</button>
                                    <?php

                                }
                                else
                                {
                                    ?>
                                    <button class="btn btn-white" type="submit">Cancel</button>
                                        <button class="btn btn-primary" onclick="payment_link()" type="button">Create</button>
                                    <?php

                                }
                                ?>
                                <span id="link"></span>
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                
