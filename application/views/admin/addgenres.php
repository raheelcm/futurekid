<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Add Client</h2>
	</div>
	<div class="col-lg-2">

	</div>
</div>

<div class="ibox-content">
	<form action="<?=base_url('admin/genres/register');?>/<?=(isset($edit))?$edit->UserID:'0';?>" method="post" class="form-horizontal">
		
		<?php $this->load->view('flash') ?>
		<div class="form-group"><label class="col-sm-2 control-label">First Name</label>

			<div class="col-sm-10"><input type="text" name="first_name" value="<?=(isset($edit))?$edit->first_name:'';?>" class="form-control"></div>
		</div>
		<div class="form-group"><label class="col-sm-2 control-label">Last Name</label>

			<div class="col-sm-10"><input  value="<?=(isset($edit))?$edit->last_name:'';?>" type="text" name="last_name" class="form-control"></div>
		</div>
		<div class="form-group"><label class="col-sm-2 control-label">Email</label>

			<div class="col-sm-10"><input type="text"  value="<?=(isset($edit))?$edit->email:'';?>" name="email" class="form-control"></div>
		</div>
		<div class="form-group"><label class="col-sm-2 control-label">Username</label>

			<div class="col-sm-10"><input type="text" name="uname"  value="<?=(isset($edit))?$edit->uname:'';?>" class="form-control"></div>
		</div>
		<?php
		if(!isset($edit))
		{
			?>
			<div class="form-group"><label class="col-sm-2 control-label">Password</label>

			<div class="col-sm-10"><input type="text" name="upass" class="form-control"></div>
		</div>
		<div class="form-group"><label class="col-sm-2 control-label">Confirm Password</label>

			<div class="col-sm-10"><input type="text" name="cpass" class="form-control"></div>
		</div>
		<div class="form-group"><label class="col-sm-2 control-label">Sandbox</label>

			<div class="col-sm-10"><input type="checkbox" name="env" value="1" class="form-control"></div>
		</div>
			<?php
		}
		?>
		<div class="form-group">
			<label class="col-sm-2 control-label">Gateway</label>
			<div class="col-sm-10">
				
				<select  class="form-control"  name="geteway">
				<?php 
					if($data)
					{
						foreach ($data as $key) 
						{
							?>
								<option value="<?php echo $key['tagID']; ?>" <?=(isset($edit) && $edit->gate_way_ID == $key['tagID'])?'selected':'';?>><?php echo $key['name']; ?></option>
							<?php
						}
					}
				?>
				</select>		
			</div>
		</div>








		<div class="hr-line-dashed"></div>
		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-2">
				<button class="btn btn-white" type="submit">Cancel</button>
				<button class="btn btn-primary" type="submit"><?=(!isset($edit))?'Add':'Update';?> Client</button>
			</div>
		</div>
		</form>
	</div>
