<?php
$CI=&get_instance();
$group= $CI->db->where('status',1)->get('groups')->result_array();
$books= $CI->db->where('status',0)->get('links')->result_array();
$tags= $CI->db->where('status',0)->get('tags')->result_array();
$genres= $CI->db->where('status',0)->get('genres')->result_array();
$users= $CI->db->where('status',1)->get('users')->result_array();

?>

        
            <div class="wrapper wrapper-content">
        <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">Total Links</span>
                                <h5>Links</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?= count($books)  ?></h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Total Gateway</span>
                                <h5>Gateways</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?= count($tags) ?></h1>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        
        
        
        
        
         <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">Total Users</span>
                                <h5>Users</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?= count($users); ?></h1>
                            </div>
                        </div>
                    </div>
                   
                   
                   
        </div>
                </div>
        